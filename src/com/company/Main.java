package com.company;

public class Main {

    public static void main(String[] args) {
        int a = 1;
        int b = -2;
        int c = -3;
        double x, x1, x2;
        double discriminar;
        discriminar = b * b - 4 * a * c;
        if (discriminar < 0) {
            System.out.println("Корней в уравнении  нет");
        } else if (discriminar == 0) {
            System.out.println("Есть один корень уравнения: ");
            x = (-b)/(2*a);
            System.out.println("Корень уравнения = "+x);
        } else if (discriminar > 0) {
            System.out.println("Есть два корня уравнения");
            x1 = ((-b)+Math.sqrt(discriminar))/(2*a);
            System.out.println("Первый корень уравнения= "+x1);
            x2 = ((-b)-Math.sqrt(discriminar))/(2*a);
            System.out.println("Второй корень= "+x2);
        }

    }
}

